import { shallowMount, mount, createLocalVue } from '@vue/test-utils'
import GameOfThronesHouses from '@/views/GameOfThronesHouses'
import  Vuex from 'vuex'


const localVue = createLocalVue()
localVue.use(Vuex)
describe('GameOfThronesHouses', () => {

  test("testing the div",()=>{
    const wrapper= mount(GameOfThronesHouses);
    expect(wrapper).toMatchSnapshot();
  })
})