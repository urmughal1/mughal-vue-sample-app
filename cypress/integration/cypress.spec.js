/**
 * Home page test case
 */
describe('Single page application', () => {

  it('Root Page of the App', () => {
    cy.visit('/')

    cy.wait(5000)
  })
});

/**
 * Checking the API URL Before Listing the Houses test case
 */
describe('Checking the API Before Listing the Houses', () => {
  it('Its Working', () => {

    cy.visit('https://www.anapioficeandfire.com')

    cy.wait(5000)
  })
});

/**
 * House Detail page test case
 */
describe("Is loading Project Detail page? ",()=>{
  it("House Detail Page",()=>{

    cy.visit("http://localhost:8080/")

    cy.contains('Click Here for More Details').click()

    cy.url().should('include','/house-detail')

    cy.wait(5000)
  })
})

/**
 * Base url page test case
 */
describe('Test the Base Url ', () => {

  it('Home Page Loaded', () => {
    cy.visit('/')

    cy.wait(5000)
  })
})

/**
 * Testing the detail on the House-Detail Page test case
 */
describe("Testing the detail on the House-Detail Page",()=>{
  it("House-Detail Page",()=>{

    cy.visit("http://localhost:8080/")

    cy.contains('Click Here for More Details').click()

    cy.contains("Founded")
    cy.contains("Region")
    cy.contains("Founder")
    cy.contains("Coat Of Arms")
    cy.contains("Words")
    cy.contains("Died Out")
    cy.contains("Titles")
    cy.contains("Seats")
    cy.contains("Ancestral Weapons")
    cy.contains(" Current Lord")
    cy.contains("Heir")
    cy.contains("URL")
    cy.contains("Overlord")
    cy.contains("Cadet Branches")
    cy.contains("Sworn Members")

    cy.wait(5000)
  });
});

/**
 * Checking the Back button on the House-Detail Page test case
 */
describe('Checking the Back button', () => {
  it('Its Working', () => {
    cy.visit('/')

    cy.contains('Click Here for More Details').click()

    cy.url().should('include','/house-detail')

    cy.contains("Back To List").click()

    cy.wait(5000)
  })
});
