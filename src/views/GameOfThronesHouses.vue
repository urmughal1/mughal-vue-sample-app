<template >
  <div>
    <!-- Overlay for interactive message -->
    <b-overlay :show="iLoading" rounded="lg" spinner-type="grow" variant="info" class="overlay-box">

      <!-- Searching Through the Houses-->
      <div class="form-group" >
        <input class="form-control bg-warning" type="text" v-model="sSearch" placeholder="Search by Name Please ...."> </input>
      </div>

    <!-- this div used to detect the scrolling area -->
    <div class="list-group" id="infinite-list">

      <!-- Accordion area -->
      <div class="accordion" role="tablist">

        <!-- list iteration -->
        <div v-for="(item,index) in getHouses" :key="index">
          <b-card no-body class="mb-1 text-left">
            <b-card-header header-tag="header" class="p-1" role="tab">
              <b-button block v-b-toggle="'text'+index" variant="white" class="text-left"><h6> <code>{{ (index+1)+":-  "}}</code>{{item.name}}</h6></b-button>
            </b-card-header>
            <b-collapse :id="'text'+index" visible accordion="accordion1" role="tabpanel">

              <!-- to show some data inside the body -->
              <b-card-body>
                <b-card-text>
                  <div>
                    <h3>{{ item.name }}</h3>
                    <b-button  block
                        variant="info"
                        class="text-center w-25 float-right"
                        @click="rHouseDetail(item)"
                    >
                      Click Here for More Details
                    </b-button>
                  </div>
                  <h6>Region: {{ item.region }}</h6>
                  <h6>Founded: {{ item.founded ?item.founded:"None"}}</h6>
                  <h6>URL: {{ item.url ?item.url:"None"}}</h6>
                </b-card-text>
              </b-card-body>

            </b-collapse>
          </b-card>
        </div>
      </div>
    </div>
    </b-overlay>
  </div>
</template>

<script>

import axios from "axios";

export default {

name: "GameOfThronesHouses",

  data(){
    return{
      iLoading:0,
      sSearch: '',
    }
  },

  computed:{

    /**
     * getting the latest List of houses, filtered if sSearch string is avaiable
     */
    getHouses() {

      const aHouses=this.$store.getters.getHouses;
      if(this.sSearch){
        return aHouses.filter(elm=>(elm.name.toLowerCase().includes(this.sSearch.toLowerCase())));
      }
      return aHouses;
    },

    /**
     * getting the latest page courser
     */
    getPageCourser() {
      return this.$store.getters.getPageCourser;
    },
  },

  methods: {

    /**
     * updating the page courser from the store
     * @iPage {int}
     */
    updatePageCourser(iPage) {
      this.$store.dispatch('setPageCourser', iPage);
    },

    /**
     * updating the House array from the store
     * @aHouses {Array}
     */
    updateHouses(aHouses) {
      this.$store.dispatch('addHouses', aHouses);
    },

    /**
     * Handling the redirection
     * @item {Object}
     */
    rHouseDetail(item) {
      this.$router.push({name: 'HouseDetail', params: {house: item.name.replace(/\s+/g, '')}})
    },

    /**
     * Api call section
     * getting the houses from https://www.anapioficeandfire.com/api/houses
     * this api dont have max Number of pages and which page we are currently we are on.
     * when we make an API by our own then we have the information to handle the pagination better.
     * A Fake overlay handling can be replaced with original API Call
     */
    loadHousesData () {

      this.iLoading = this.iLoading + 1;

      setTimeout(()=>{
        const iPageSize = 25;
        const iPage = this.getPageCourser;
        const sURL = 'https://www.anapioficeandfire.com/api/houses';

        axios
        .get(sURL+'?page='+(iPage)+'&pageSize='+(iPageSize))
        .then(response =>{ this.updateHouses(response.data); })
        .catch((error) => {

          if (error.response) {
            console.error(error.response.data);
            console.error(error.response.status);
            console.error(error.response.headers);
          }
          else if (error.request) { console.error(error.request); }
          else { console.error('Error', error.message); }
          console.error(error.config)}
        );

        if(this.getPageCourser<=22) this.updatePageCourser(iPage+1);
        else alert("List is reaching its limit");

        this.iLoading = this.iLoading - 1;

      },1000)


    },
  },

  mounted () {

    /**
     * Detect when scrolled to bottom.
     */
    const listElements = document.querySelector('#infinite-list');

    /**
     * Anchor Dropping .
     */
    listElements.scrollTop = listElements.clientHeight;
    /**
     * Event Listener to scrolling
     */
    listElements.addEventListener('scroll', () => {
      if(listElements.scrollTop + listElements.clientHeight >= listElements.scrollHeight) {
        this.loadHousesData();
      }
    });

    /**
     * initialization: when user visit the page first time
     */
    if(this.getPageCourser===0) this.updatePageCourser(1);
    this.loadHousesData();
  },
}
</script>

<style scoped>

.list-group {
  overflow: auto;
  height: 85vh;
  border: 2px solid #dce4ec;
  border-radius: 5px;
}

</style>