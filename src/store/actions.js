import * as types from './mutation-types';

/**
 *  ACTIONS
 *  are a bridge used to mutate the state
 *  and eventually perform other number crunching before doing so
 */

//pragma mark - Houses

export const setHouses = (state, aHouses) => {

  state.commit(types.SET_HOUSES, aHouses);
}

export const addHouses = (state, aHouses) => {

  state.commit(types.ADD_HOUSES, aHouses);
}

//pragma mark - page courser
export const setPageCourser = (state, iPageCourser) => {

  state.commit(types.SET_PAGE_COURSER, iPageCourser);
}

