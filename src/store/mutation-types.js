/**
 * MUTATION-TYPES
 * are a commonly seen pattern
 * allows to overview what mutation are possible in the whole app
 */

//pragma mark - User

/** Houses mutation types */
export const SET_HOUSES = 'set_houses';
export const ADD_HOUSES = 'add_houses';


/** page courser mutation types */
export const SET_PAGE_COURSER = 'set_page_courser';
