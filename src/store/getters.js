/**
 * GETTERS
 * are for getting the state
 * but also processed data based on the state
 */

// return the Houses array
export const getHouses = (state) => {

  return state.aHouses;
}

// return the page courser
export const getPageCourser = (state) => {

  return state.iPageCourser;
}
