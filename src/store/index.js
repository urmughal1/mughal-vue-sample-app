import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';
import * as getters from './getters';
import * as actions from './actions';
import mutations from './mutations';


Vue.use(Vuex);

export const emptyStore = {
  aHouses: [],
  iPageCourser:0,
};

const state = JSON.parse(JSON.stringify(emptyStore));

export default new Vuex.Store({
  state,
  getters,
  actions,
  mutations,
  plugins: [createPersistedState()]
});
