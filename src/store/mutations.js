import * as types from './mutation-types';

/**
 * MUTATIONS
 * are to mutate the state directly
 */

export default {

  //pragma mark - Houses

  /* set houses*/
  [types.SET_HOUSES](oState, aHouses) {

    oState.aHouses = aHouses;
  },

  /* add houses*/
  [types.ADD_HOUSES](oState, aHouses) {

    aHouses.forEach(elm=>{
      if(!oState.aHouses.find(item=>item.name===elm.name)){
        oState.aHouses.push(elm);
      }
    })
  },

  //pragma mark - Page Courser

  /* set Courser*/
  [types.SET_PAGE_COURSER](oState, iPageCourser) {

    oState.iPageCourser = iPageCourser;
  },

};