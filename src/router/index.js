import Vue from "vue";
import VueRouter from "vue-router";
import GameOfThronesHouses from "../views/GameOfThronesHouses.vue";
import HouseDetail from "../views/HouseDetail.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "GameOfThronesHouses",
    component: GameOfThronesHouses,
  },
  {
    path: "/house-detail/:house",
    name: "HouseDetail",
    component: HouseDetail,
    meta:{fail: '/'}
  },
];

export default new VueRouter({
  mode: "history",
  routes:routes,
});

