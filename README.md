# Umer Mughal
## Vue Web App

### Installation
#### NOTE: please use the "master" branch to run all configuration
please follow the following steps
- Clone the repository into your local PC
```sh
$ git clone https://gitlab.com/urmughal1/mughal-vue-sample-app.git
```
- Go to the root directory and run the start.sh
```sh
$ ./start.sh
```
OR
-  Go to the root directory of the application
-  And please run the following commands
```sh
$ npm install
$ npm run start
```
and that's it. To see the application in action please go to this address http://localhost:8080/

#### Docker installation
I prepared the docker configuration as well.
- Clone the repository into your local PC
- Please run the following command from the root directory
```sh
$ docker-compose up -d 
OR
$ docker-compose up --build
```
To see the application in action please go to this Address 
```sh
http://localhost:3000/
```

### introduction
This is Vue.js based single page application. In this app, users can see the list of Game of Thrones Houses
from an open-source API ( https://anapioficeandfire.com/ ). The link was given in the assignment.

##### Note: Application is tested and fully functional over linux operating system
### Architecture
The architecture of the application is so simple. it consists of three main components: App.vue ( Main vue component ),
GameOfThronesHouses.vue ( Displaying the List ), and HouseDetail.vue ( showing the detailed information of one house ).

GameOfThronesHouses.vue This is the front page(root page for user) where I am loading the List of Houses.
A list of accordions will appear and when you click on the accordion you can see a small detail of the house, for further
detail you can click on the button inside the accordion.

### Strategy/Approach

Rules for me
1. concept (search, plan and prepare the documentation )
2. develop (write clean and commented code)
3. test ( unit, integration, regression, and user acceptance testing )

To solve the problem, Firstly I prepared the Structure of the directories then I installed the dependencies.
The most important thing was how and when to load the data and when the user leaves the page then don't load the data from start.

To answer those questions I used vuex which is active and persistent storage of the browser.
I am storing the array of houses in a vue store and then load the latest data from the vuex.
A page course is also placed inside of the store so fetching starts where the user left.
to answer the question of when to load, I used the endless scrolling technique which I implemented without any library.
user will scroll and the app will load the data. In the end, there will be an alert where I will display the message
that this is the end of results from the online API side.

I developed a fake overlay on top of the page when frontend is making an API call then I need to show a proper
message that will tell the user that API is fetching data form an outsource. I also tried to dropping an
anchor to start where user left last time.

There is a small search functionality can be found as well you can also search through the houses.

I also used some frontend CSS libraries like bootstrap-vue.

Some features of the APP
- App can be Extended very easily
- Hungarian notation as coding style
- Commented and cleaned code
- Persistence storage used
- Endless scrolling
- Anchor Dropping
- Overlay
- responsive/simple design
- search through the Houses

### Testing
I am using the cypress tool for testing which is very interactive and providing the
full package to test as unit, integration, and interfaces.

I have created more than 5 test case and the code can be found in the following directory
```sh
<RootDir>/cypress/integration/cypress.spec.js
```
to run the test-cases please run the following commands
```sh
$ npm install cypress --save-dev
$ ./node_modules/.bin/cypress run
```
OR
```sh
$ ./testing.sh
```
Results are visible on terminal and cypress also create an interactive
video that can be found in the following directory after running the above command.
```sh
<RootDir>/cypress/videos/cypress.spec.mp4
```

### Production
For production, you need to run the following command
```sh
$ npm run build
```
after running this command you will see a dist folder that will contain
the distributable

I have also created CI/CD. gitlab-ci.yaml can also be found inside the repository.(Disabled for Security Reasons)

Online Demo is available via the following web app link
```sh
http://mughal-sample-vue-app.s3-website.eu-central-1.amazonaws.com/
```
please click here to enjoy

### Core Libraries/Dependencies
- webpack
- vuex
- bootstrap-vue
- axios
- cypress

### if you have any question please contact me.

# Happy Coding :)

used image(s) are free and open-source available over the internet
